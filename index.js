// array methods
/*
    -Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
    -Array can be either mutated or iterated
        - Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/


// mutator methods
// functions / methods that mutate or change an array
// these manipulate the original array performing task such as adding and removing elements.

console.log("==Mutator Methods==");
console.log("------------------");
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];


//push()
/* 
	-adds an element in the end of an array AND returns the new array's length.
-Syntax:
	arrayName.push[newElement];

*/

console.log("=>push()");
console.log("fruits array: ");
console.log(fruits);

let fruitsLength = fruits.push("Mango", "melon");
console.log('size/length of fruits array: ' + fruitsLength);
console.log('Mutated array from push("mango")');
console.log(fruits);

fruits.push("Avocado", "Guava");
console.log('Mutated array from push("avocado", "guava")');
console.log(fruits);


// pop()

/*
	-Removes the last element in an array and run
*/


console.log("--------------");

console.log("=>pop()");

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("mutated Array from pop method: ");
console.log(fruits);


// unshift

/*
    - Add one or more elements at the beginning of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/

console.log("-------------");
console.log("=> unshift");

fruits.unshift("Lime", "Banana");
console.log('Mutated array from unshift("Lime", "Banana")')
console.log(fruits);

// shift()

/*
	-removes an element at the beginning of an array AND returns the removed statement
*/

console.log("-------------");
console.log("=>shift");
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);

/*
    - simulatanously removes an element from a specified index number and adds new elements.
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

console.log("----------");
console.log("=>splice()");

// we could also use splice to remove an element/s
fruits.splice(1, 2, "lime", "Cherry");
console.log("Mutated array from splice method: ");
console.log(fruits);


// sort();
/*
    -Rearranges the array elements in alphanumeric order
    -Syntax:
        - arrayName.sort();
*/

console.log("-------------");
console.log("=> sort()");
fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);


// reverse();
/*
	-rearranges the array elements in reverse order
	-Syntax:
		arrayName.reverse();
*/

console.log("------------");
console.log("=>revserse()")
fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);


// Non-mutator
/*
	- Non-mutator methods are functions that do not modify or change an array after they're created.

*/

console.log("-------");
console.log("-------");
console.log("=Non-mutator methods==");
console.log("--------");
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];


// indexOf()
/*
    - Returns the index of the first matching element found in an array.
    - If no match was found, the result will be -1.
    - The search process will bne done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/


/*console.log("=>indexOf()");
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf('PH')): "+firstIndex );*/

let firstIndex = countries.indexOf("PH", 2); // with a specified index to start
console.log(firstIndex);

// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array.
    - The search from process will be done from the last element proceeding to the first element.
    - Syntax:
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
*/

console.log("-----------");
console.log("=>lastIndexOf()");
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf('PH'): " +lastIndex);

// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/
console.log("-----------");
    console.log("=>slice()");

    console.log("Original countries array:");
    console.log(countries);

    //slicing off elements from specified index to the element


  let sliceArrayA = countries.slice(2);
  // ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
  console.log("Result from slice(2)");
  console.log(sliceArrayA);


  // Slicing off element from specified index to another index. But the specified last index is not included in the return.
    let sliceArrayB = countries.slice(2, 5); //2 -> 4 // ends with the last declared index minus one
    console.log("Result from slice(2, 5):");
    console.log(sliceArrayB);

let sliceArrayC = countries.slice(-3);
    console.log("Result from slice method:");
    console.log(sliceArrayC);


   // toString

   /*	
		-return an array as a sring, seperated by commas.
		-Syntax
			arrayName.toString();
   */

console.log("-----------");
console.log("=>toString()");

// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);
console.log(typeof stringArray); //to check if the array is converted to string.